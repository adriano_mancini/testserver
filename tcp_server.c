/*
    C socket server example, handles multiple clients using threads
    Compile
    gcc server.c -lpthread -o server
*/
 
#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread
#include<time.h>

 
//the thread function
void *connection_handler(void *);
 
int main(int argc , char *argv[])
{
    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
     
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    const char* s = getenv("PORT_HACKED_APACHE");
    printf("PORT: %s",s);
    int port;
    sscanf(s,"%d",&port);
    server.sin_port = htons( port );
     
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
     
    //Listen
    listen(socket_desc , 3);
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
     
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
	pthread_t thread_id;
	
    while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        puts("Connection accepted");
         
        if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &client_sock) < 0)
        {
            perror("could not create thread");
            return 1;
        }
         
        //Now join the thread , so that we dont terminate before the thread
        //pthread_join( thread_id , NULL);
        puts("Handler assigned");
    }
     
    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }
     
    return 0;
}
 
/*
 * This will handle connection for each client
 * */
void *connection_handler(void *socket_desc)
{
	unsigned char *buf;
    //Get the socket descriptor
    int sock = *(int*)socket_desc;
    int read_size;
    int c = 0;
    char *message , client_message[2000];
buf = (char*)malloc(sizeof(char)* 4096);

char *reply = 
"HTTP/1.1 200 OK\n"
"Date: Thu, 19 Feb 2009 12:27:04 GMT\n"
"Server: ApacheHacked/0.0.1\n"
"Last-Modified: Tue, 04 Jun 2019 16:05:58 GMT\n"
"Content-Type: text/html\n"
"Content-Length: 16\n"
"Accept-Ranges: bytes\n"
"Connection: close\n"
"\n";

    //Receive a message from client
    while( (read_size = recv(sock , buf , 2000 , 0)) > 0 )
    {
	for(int i = 0; i < read_size;i++){
		//printf("%d %d\n",buf[i],c);

		if(buf[i] == 0x0D || buf[i] == 0x0A){
			switch(c){
				case 0:
					if(buf[i] == 0x0D);
						c++;
					break;
                                case 1:
                                        if(buf[i] == 0x0A);
                                                c++;
                                        break;
                                case 2:
                                        if(buf[i] == 0x0D);
                                                c++;
                                        break;
                                case 3:
                                        if(buf[i] == 0x0A);
                                                c++;
                                        break;
              

			}
		}

	
		if(c ==  4){
			printf("end of request\n");
			memcpy(buf,reply,strlen(reply));
			sprintf(buf,"%s%016d",reply,(int)time(NULL));
			write(sock,buf,strlen(buf));
		}

		if(buf[i] != 0x0D && buf[i] != 0x0A)
                      c = 0;



		
		   
	}
        //end of string marker
		//client_message[read_size] = '\0';
		
		//Send the message back to client
        //write(sock , client_message , strlen(client_message));
		
		//clear the message buffer
	//	memset(client_message, 0, 2000);
    }
     
    if(read_size == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("recv failed");
    }
         
    return 0;
} 
